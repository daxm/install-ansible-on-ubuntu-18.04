#!/usr/bin/env bash

#sudo apt-get update
#sudo apt-get install -y software-properties-common
#sudo apt-add-repository --yes --update ppa:ansible/ansible
#sudo add-apt-repository universe
#sudo apt-get install -y ansible

echo 'deb http://ppa.launchpad.net/ansible/ansible/ubuntu zesty main' | sudo tee /etc/apt/sources.list.d/ansible.list
curl -sL "http://keyserver.ubuntu.com/pks/lookup?op=get&search=0x93C4A3FD7BB9C367" | sudo apt-key add
sudo su -c 'apt-get update && apt-get install -y ansible'
sudo apt install python-pip && apt install python3-pip
sudo apt install ansible -y --allow-change-held-packages
sudo apt autoremove -y
