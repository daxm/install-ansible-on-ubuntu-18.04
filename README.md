# Install Ansible on Ubuntu 18.04

A simple script for installing Ansible on Ubuntu 18.04

## Steps
1. ```git clone https://gitlab.com/daxm/install-ansible-on-ubuntu-18.04.git```
1. ```cd install-ansible-on-ubuntu-18.04```
1. ```./install_ansible.sh```
